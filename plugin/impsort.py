import sys
import re

START_IMPORT_RE = re.compile(r'^\s*import')
END_IMPORT_RE = re.compile(r'''from\s+(["'].+["'])''')

def parse(lines):
  '''Parses the lines into import declarations.'''
  imports = []
  blank_count = 0

  for line in lines:
    sline = line.strip()
    srline = line.rstrip()
    is_start = START_IMPORT_RE.search(sline)
    is_end = END_IMPORT_RE.search(sline)

    if not sline:
      blank_count += 1
    elif is_start and is_end:
      imports.append([srline])
    elif is_start:
      current_import = [srline]
    elif is_end:
      current_import.append(srline)
      imports.append(current_import)
      current_import = []
    else:
      current_import.append(srline)

  return (imports, blank_count)

def get_import_path(imp):
  path = END_IMPORT_RE.search(imp[-1]).group(1)[1:-1]
  # Put npm modules at the top
  # package modules next
  # modules from relative directories next
  # modules from CD last
  if path.startswith('../'):
    return '3{}'.format(path)
  if path.startswith('./'):
    return '4{}'.format(path)
  if path.find('/') >= 0:
    return '2{}'.format(path)
  return '1{}'.format(path)

def main(lines):
  '''Parse, sort and print out imports'''
  (imports, blank_count) = parse(lines)
  sorted_imports = sorted(imports, key=get_import_path)
  sorted_lines = ['\n'.join(i) for i in sorted_imports]
  return '\n'.join(sorted_lines) + ('\n' if blank_count > 0 else '')

if __name__ == '__main__':
  print(main(sys.stdin))
