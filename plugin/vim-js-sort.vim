let s:script_dir = expand('<sfile>:p:h')

function! JSImportSort() range
  let s:path = shellescape(s:script_dir . '/impsort.py')
  let s:cmd = ":'<,'>! python " . s:path
  exec s:cmd
endfunction

function! JSPropSort() range
  let s:path = shellescape(s:script_dir . '/propsort.py')
  let s:cmd = ":'<,'>! python " . s:path
  exec s:cmd
endfunction

command! -range JSImportSort <line1>,<line2>call JSImportSort()
command! -range JSPropSort <line1>,<line2>call JSPropSort()
