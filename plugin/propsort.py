import sys

def main(lines):
  props = []
  comment_lines = []
  prop_lines = []

  for line in lines:
    sline = line.strip()
    if sline.startswith("/*"):
      prop = create_prop(prop_lines, comment_lines)
      if prop:
        props.append(prop)
      comment_lines = [line]
      prop_lines = []
    elif sline.startswith("*"):
      comment_lines.append(line)
    elif sline.endswith("*/"):
      comment_lines.append(line)
    else:
      prop_lines.append(line)

  prop = create_prop(prop_lines, comment_lines)

  if prop:
    props.append(prop)
  props.sort()

  all_lines = []
  for (_, prop_lines, comment_lines) in props:
    for comment_line in comment_lines:
      all_lines.append(comment_line.rstrip())
    for prop_line in prop_lines:
      all_lines.append(prop_line.rstrip())

  return '\n'.join(all_lines)


def create_prop(prop_lines, comment_lines):
  if prop_lines:
    return (prop_lines[0], prop_lines, comment_lines)
  return None


if __name__ == "__main__":
  print(main(sys.stdin))
